$('#fileup').change(function () {
    //here we take the file extension and set an array of valid extensions
    var res = $('#fileup').val();
 
    var arr = res.split("\\");
    var filename = arr.slice(-1)[0];
    filextension = filename.split(".");
    filext = "." + filextension.slice(-1)[0];
    valid = [".doc", ".docx", ".txt"];
    //if file is not valid we show the error icon, the red alert, and hide the submit button
    if (valid.indexOf(filext.toLowerCase()) == -1) {
       $(".imgupload").hide("slow");
       $(".imgupload.ok").hide("slow");
       $(".imgupload.stop").show("slow");
 
       $('#namefile').css({ "color": "red", "font-weight": 700 });
       $('#namefile').html("File " + filename + " is not  pic!");
 
       //   $( "#submitbtn" ).hide();
       //   $( "#fakebtn" ).show();
       document.getElementById("submit").disabled = true;
    } else {
       //if file is valid we show the green alert and show the valid submit
       $(".imgupload").hide("slow");
       $(".imgupload.stop").hide("slow");
       $(".imgupload.ok").show("slow");
 
       $('#namefile').css({ "color": "#89EBCB", "font-weight": 600 });
       $('#namefile').html(filename);
 
       //   $( "#submitbtn" ).show();
       //   $( "#fakebtn" ).hide();
       document.getElementById("submit").disabled = false;
    }
 });
 
 function submitSRS() {
    console.log("submit ")
    let formData = new FormData()
    var x = document.getElementById("fileup");
    //  console.log(x.files[0])
    formData.append('file', x.files[0])
    fetch('http://localhost:5000', {
       method: 'POST',
       body: formData
    })
       .then(resp => {
          resp.json().then(function (data) {
             if (data.status == true) {
                document.getElementById("download").disabled = false;
                console.log(data)
             }
          });
       })
 }
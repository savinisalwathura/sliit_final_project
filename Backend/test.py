# Using the Stanford POS Tagger in NLTK
import os
from nltk.tokenize import word_tokenize
from nltk.parse import stanford
from nltk.tag import StanfordPOSTagger
from nltk.stem.lancaster import LancasterStemmer
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
import operator
from flask import Flask, send_from_directory, abort,jsonify,make_response, request
from flask_cors import CORS
app = Flask(__name__)
CORS(app)
app.config['UPLOAD_FOLDER'] = "./data"

# A function to read the text file and save it as a string
def read_file(path):
 file=open(path,"r",encoding="UTF8") # opening a file
 text=file.read() # read it and save it as a string
 file.close() # close the file
 return text

# A function to split the data into tokens 
def tokenize_text(text):
 tokens = word_tokenize(text) # tokenize into words
 return tokens

# A function to create a tagger using StanfordPOSTagger
def make_tagger():
 stanford_dir = './stanford-postagger-2018-10-16' # change it into your own path
 modelfile = stanford_dir+"/models/english-bidirectional-distsim.tagger" # model file
 jarfile=stanford_dir+"/stanford-postagger.jar"# jar file
 st=StanfordPOSTagger(model_filename=modelfile, path_to_jar=jarfile) # create a tagger
 return st

def steam_tokens(tokens):
 lancaster=LancasterStemmer() 
 snowball=SnowballStemmer("english") 
 stem_sentence=[]
 for word in tokens:
   stem_sentence.append(snowball.stem(word))
 return stem_sentence  

def get_nouns(tokens): 
 nounes = [(word, tag) for word, tag in tokens if tag in ('NN')]    
 return nounes

def get_verbes(tokens): 
 verbes = [(word, tag) for word, tag in tokens if tag in ('VBP')]    
 return verbes 

# A function to get a dictionary of words' frequencies
def get_freq(tagged): 
 freq_dist={}
 # getting frequencies using for loop
 for word,tag in tagged:
  if word in freq_dist:
   freq_dist[word]+=1
  else:
   freq_dist[word]=1

# sort the frequencies (It can be skipped. If you want the list of sorted frequencies, it might be helpful.)
 sorted_freq_dist=sorted(freq_dist.items(),key=operator.itemgetter(1))
# change into the dictionary since it is easier to approach
 dict_sorted_freq_dist=dict(sorted_freq_dist)
 return dict_sorted_freq_dist

# A function to remove th stop words
def remove_stopwords(tokens):
# Remove the stop words by using the english stop words provided by NLTK 
 e_stopwords= set(stopwords.words('english'))
 clean_tokens= [tok for tok in tokens if len(tok.lower())>1 and (tok.lower() not in e_stopwords)]
 return clean_tokens

# A function to tag the words and save the result as a file
def tag_and_save(tagger, tokens, path):

 clean_tokens = remove_stopwords(tokens) # First, remove the stop words and get the clean tokens
 steamed_tokens = steam_tokens(clean_tokens)

 tagged_data = tagger.tag(steamed_tokens) # Tag the tokens with the tagger
 nouns_data = get_nouns(tagged_data)
#  verbs_data = get_verbes(tagged_data)

 freq_tagged_data=get_freq(nouns_data) # The dictionary of words' frequencies
#  freq_verbes_data=get_freq(verbs_data) # The dictionary of words' frequencies
 
 # tagged_data = sorted(tagged_data,key=operator.itemgetter(1)) # if you want to group the same tags, sort it
 agged_data = list(set(nouns_data)) # if you don't want duplicated words, change it into set
#  agged_verbes_data = list(set(verbs_data)) # if you don't want duplicated words, change it into set

 file=open(path,"w",encoding="UTF8") # open a file to save
 # print the result in a file with the format "word/tag (frequency=num)"
 for word, tag in agged_data:
  file.write(word+"/"+tag+" (frequency="+str(freq_tagged_data[word])+")\n")

#  for word, tag in agged_verbes_data:
#   file.write(word+"/"+tag+" (frequency="+str(freq_verbes_data[word])+")\n") 

 file.close() # close the file

# A function to call all the functions above
def stanford_pos_tagger(file_to_read, file_to_save):
 text=read_file(file_to_read) # read the file
 tokens=tokenize_text(text) # split into tokens
 st = make_tagger() # create a tagger
 tag_and_save(st, tokens, file_to_save) # give the tagger, the tokens and the path of file to save as parameters for tagging and saving

@app.route('/', methods=['GET', 'POST'])
def upload_file():
  file = request.files['file']
  file.save(os.path.join(app.config['UPLOAD_FOLDER'], "read.txt"))
  stanford_pos_tagger("./data/read.txt","./data/write.txt")
  message = {"status" : True, "statusText" : "file uploaded"}
  return make_response(jsonify(message), 200)

@app.route('/download', methods=['GET', 'POST'])
def download_file(): 
  try:
    return send_from_directory(app.config["UPLOAD_FOLDER"], filename="write.txt", as_attachment=True,mimetype="text/plain")
  except FileNotFoundError:
        abort(404)

# main function
if __name__ == '__main__':
  app.run()
#  stanford_pos_tagger("./data/read.txt","./data/write.txt") # paths of file to read and to save (file to read must be an existing one, change the paths into your own ones)